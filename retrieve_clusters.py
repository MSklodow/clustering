#!/usr/bin/env python3

import sys, glob
from pybioshell.core.data.io import read_fasta_file, create_fasta_string

info = """
Reads cluster files (named c*_*) and a .fasta file; produces s*_* files

Each c*_* contains only sequence ids for a cluster, e.g.:
# medoid distance max, average, stdev: 153 47.5 61.5488
# medoid element 6d93A
2melA 25
6d93A 0
6d8yA 12
1yylM 153

The output s*_* file for that cluster looks as below:
> 2melA
GSAFCNLRACELSCRSLGLLGKCIGEECKCVPY
> 6d93A
AFCNLRRCELSCRSLGLLGKCIGEECKCVPA
> 6d8yA
AFCNLRRCELSCRSLGLLGKCIGEECKCVPH
> 1yylM 1yylS 1yymM 1yymS
NLHFCQLRCKSLGLLGKCAGSFCACX

USAGE:

    python3 retrieve_clusters.py input.fasta [clusters_dir]
    
The default name for a directory with c*_* files is "clusters"
"""

if len(sys.argv) == 1:
    print(info)
    sys.exit(0)
    
fasta = read_fasta_file(sys.argv[1])
# --- Look, what a nice dict-comprehension I made! It takes a list of Sequence objects and turns them into a dict
fasta_dict = {s.header().split()[0]: s for s in fasta}

c_dir = "clusters" if len(sys.argv) < 3 else sys.argv[2]

# --- for every cluster file we produce a file with its sequences
for cluster_file in glob.glob(c_dir+"/c*_*"):
  
  name_tokens = cluster_file.split("/")
  name_tokens[-1] = "/s" + name_tokens[-1][1:]
  new_name = "".join(name_tokens)
  print("processing",cluster_file,"->",new_name)

  out_file = open(new_name,"w")
  for line in open(cluster_file):
    if len(line) <1 or line[0]=="#" : continue
    seq_id = line.split()[0]
    seq = fasta_dict[seq_id]
    print("> " + seq.header()+"\n"+seq.sequence,file=out_file)
  out_file.close()


