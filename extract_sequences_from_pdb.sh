#/bin/bash
lista=$1

# MSklodowski path
SEQC_DIR="~/bioshell/doc_examples/cc-examples/apps/seqc-fasta_from_pdb"
# dgront path
SEQC_DIR="/home/dgront/src.git/bioshell/bin/"

for k in `cat $lista`
do
    cp /mnt/storage/DATABASES/PDB_MIRROR/wwpdb/$k ./$k
    i="$( echo $k | sed 's/pdb//' )"
    j="$( basename $i .ent.gz)"
    echo $j
    echo $i
    gunzip -c  $k > $j.pdb
    $SEQC_DIR/seqc -ip=$j.pdb -of  -out:fasta  1>>1.0.fastadb
    rm *pdb
    rm pdb*
done
