#!/bin/bash

DIR=wwpdb

today=`date | awk '{printf "%s-%s-%s", $3,$2,$6}'`

if [ ! -e /wwpdb ] ; then
 mkdir wwpdb
fi
cd $DIR
rm -f index.html entries.idx
#wget ftp://ftp.wwpdb.org/pub/pdb/data/structures/all/pdb/ -a /dev/null
wget ftp://ftp.wwpdb.org/pub/pdb/derived_data/index/entries.idx -a /dev/null
grep -v IDCODE entries.idx |  awk '{print $1}' | tr '[:upper:]' '[:lower:]' > deposits

for i in `cat deposits` 
do
  j=`echo $i | tr '[:upper:]' '[:lower:]'`
  j='pdb'$j'.ent.gz'
  if ! test -f $j ; then
    echo "Downloading" $j >> ../fresh-$today
    wget 'www.rcsb.org/pdb/files/'$i'.pdb.gz' -O $j -a WGET_ERR
  fi
done
cd ..
