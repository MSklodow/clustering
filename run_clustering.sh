#!/bin/bash

N_SEQ=1593

#---------- Run clustering
./clust -i=1.0.distance_output \
    -n=$N_SEQ \
    -clustering:max_distance=256 \
    -clustering:out:tree=seq_tree \
    -clustering:missing_distance=300

# ---------- Plot the clustering progress
#grep "@" seq_tree | grep -v ":" > distance_step_by_step

# ---------- #Extract clusters for sim = 0.5 (1-0.5)*400 = 200
#clust -i=1.0.distance_output \
#    -n=$N_SEQ \
#    -clustering:max_distance=256 \
#    -clustering:out:distance=200 \
#    -clustering:in:tree=seq_tree
