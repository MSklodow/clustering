# Krok po kroku co trzeba zrobić

## 1 Przygotowanie bazy plików fasta używanych w dalszych krokach analizy ( kroki na bioshell.pl)

**1.1**	Na bioshell w katalogu /mnt/storage/DATABASES/PDB_MIRROR znajduje się kopia całej strukturalnej bazy PDB. Jeżeli jest nieaktualna ( za pierwszym razem na pewno będzie) można odpalić znajdujący się w tym katalogu skrypt wgetPDB.sh celem uaktualnienia. 

*Zastrzeżenie do punktu 1.1: W przypadku kiedy nie pracujemy na bioshell trzeba ściągnąć PDB na dysk i korzystać z niego. Proponowaną metodą jest użycie zmodyfikowanego wgetPDB.sh znajdującego się w repozytorium pod nazwą wgetPDBmod.sh. Krok 1.2 należy wtedy wykonać z używanym adresem katalogu z depozytami PDB.*

**1.2**	Po uaktualnieniu możemy stworzyć wspólny plik dla wszystkich pdb. W tym celu proponuje w wpierw stworzyć listę wszystkich pdb, które obecnie znajdują się w /mnt/storage/DATABASES/PDB_MIRROR/wwpdb/. Następnie przenosimy się do docelowego katalogu do obliczeń i możemy sseqc.sh <w repozytorium>( skryptu do obsługi programu seqc, który musi być w tym samym katalogu). Otrzymujesz 1.0.fastadb czyli zbiorczy plik dla wszystkich depozytów pdb w formacie fasta. 

***Użycie: ./sseqc twojalistapdb***

*Zastrzeżenie: sseqc.sh działa z założeniem, że masz skompilowanego bioshella w katalogu domowym. Jeżeli jest w innym miejscu / z innego powodu wystarczy zmodyfikować ścieżkę w linii 11 sseqc.sh*

**1.3** Jeżeli analiza wymaga mniejszego podzbioru danych proponuje użyć fasta_subset.py*< w repozytorium>* do wykonania podziału z zachowaniem stosunku dłuższych sekwencji do krótszych ( statystycznie reprezentatywne podgrupy). 

***Użycie: python3 read_fasta.py ułamek_jaki_ma_być_w_outpucie 1.0.fastadb > (jakiułamek).fastadb***

*Zastrzeżenie: Na potrzeby dalszej analizy założę, że używany całości czyli 1.0.fastadb. Równie dobrze może być to 0.5.fastadb czy 0.1.fastadb.* 

**1.4** Trzeba usunąć spacje z nagłówków za pomocą bezspacji.py*< w repozytorium>* ( zamiast 1j85 A, będzie 1j85A). Otrzymujesz 1.0.bezspacji
Użycie: python3 bezspacji.py 1.0.fastadb

*Zastrzeżenie: Ten punkt można pominąć przez modyfikacje programu seqc. Nie robiłem tego z uwagi na moją nikłą umiejętność pisania w C++*

# Teraz można wybrać jaki program do lokalnego uliniowienia sekwencyjnego chcemy użyć. 
# W punktach 2.x będę opisywał podejście użyciem BLAST(psiBLAST), natomiast 3.x HMMER(jackhmmer).

# 2. Ścieżka BLAST

**2.1** Jeżeli masz dostęp do aktualnego pakietu BLAST to ten punkt można pominąć ( na bioshell /mnt/storage/TOOLS/blast+). Jeżeli nie to proponuje ściągnąć i rozpakować pakiet z źródła: https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/ z zależności od wymagań komputera. 

**2.2** Za pomocą zawartego w katalogu bin BLAST-a programu makeblastdb. Można to zrobić w katalogu z plikami fasta z uwagi na wygodę. Proponuje wszystkie pliki otrzymane z tego programu wrzucić do oddzielnego katalogu (np.1.0DB). 

***Użycie: makeblastdb -in 1.0.bezspacji -dbtype prot -title 1.0  -out 1.0***

**2.3** Przystąpmy do uliniowienia lokalnego. W tym celu w katalogu z 1.0.bezspacji powinny się znaleźć StBLA.py ( Scrips to BLAST Local Alligment), pybioshell.co, psiblast( na bioshell jest dobrze zaadresowany; w innym klastrze obliczeniowym należy poczynić zmiany w adresowaniu pliku spsyblash.sh linia 7), deleter.sh*< wszystkie w repozytorium>* oraz 1.0.bezspacji. Konfigurację można wykonać w pliku spsyblash.sh zmieniając parametry dopasowania. Powstający plik zawiera nagłówek, oraz pary kodów pdb. W wyniku pracy programu otrzymujemy 1.0.localB__output.

***Użycie: python3 StLA.py 1.0.bezspacji***

*Zastrzeżenie praktyczne: Zalecam używanie programu pod „screenem”. Jeżeli zdecydowano się nazywać bazę BLAST-a w inny sposób bądź znajduje się ona w innym katalogu należy zmienić ścieżkę w fladze -db skryptu spsyblash.sh*

# 3. Ścieżka HMMER 

**3.1** Jeżeli posiadasz dostęp do Jackhmmer krok ten należy pominąć ( na bioshell wywołuje się go po prostu z poziomu konsoli jackhmmer). Jeżeli nie tutaj znajduje się instalacja  http://hmmer.org/documentation.html. 

*Zastrzeżenie: Dalsze kroki będą realizowane z myślą o używaniu bioshell. Jeżeli używasz innego klastra/stacji należy poprawić ścieżki w skrypcie sjackhmmer.sh w linii 7.* 

**3.2** Uliniowienie w tej ścieżce wykonuje się za pomocą skryptu StHLA.py( Scrips to HMMER Local Alligment). Do jego pracy potrzeba wspominanego sjackhmmer.sh, pybioshell.co, deleter.sh*< wszystkie w repozytorium>* oraz 1.0.bezspacji. Otrzymujemy 1.0.localH__output

***Użycie: python3 StHLA.py 1.0.bezspacji***

# 4. Uliniowienie globalne

**4.1** Celem wykonania dopasowania globalnego (semiglobalnego) używamy programu pairwise_identity_for_clustering *<w repozytorium>*. Do jego użycia potrzeba listy 1.0.bezspacji oraz jednego z wyjściowych plików uliniowienia lokalnego. Trzecią wartością jest próg odcięcia sekwencyjnego, powyżej którego hity będą zapisywane w outpucie.

***Użycie: ./pairwise_identity_for_clustering 1.0.bezspacji 1.0.localH__output 4 0.28 > 1.0.globalH__output***

# 5. Analiza skupień 

**5.1** W pierwszym kroku musimy zamienić podobieństwo sekwencyjne na odległości w analizie skupień. W tym celu potrzebny jest nam skrypt  similarity_to_distance.py*<w repozytorium>*. Otrzymujemy 1.0.distance_output

***Użycie: python3.6 similarity_to_distance.py 1.0.globalH__output > 1.0.distance_output***

**5.2** Do właściwej analizy skupień  potrzebne są nam pliki 1.0.distance_output, clust*<w repozytorium>* oraz run_clustering.sh*<w repozytorium>*. Skrypt run_clustering.sh pełni 2 funkcje związku z czym jego użycie dostanie opisanie dokładnie poniżej.

**5.2.1**

```
 #!/bin/bash

N_SEQ=1593

#---------- Run clustering
./clust -i=1.0.distance_output \
    -n=$N_SEQ \
    -clustering:max_distance=256 \
    -clustering:out:tree=seq_tree \
    -clustering:missing_distance=300

# ---------- Plot the clustering progress
#grep "@" seq_tree | grep -v ":" > distance_step_by_step
    
# ---------- #Extract clusters for sim = 0.5 (1-0.5)*400 = 200
#clust -i=1.0.distance_output \
#    -n=$N_SEQ \
#    -clustering:max_distance=256 \
#    -clustering:out:distance=200 \
#    -clustering:in:tree=seq_tree
```

Skrypt składa się z 2 głównych („Run clustering” oraz „Extract clusters for sim”) i jednego potocznego akapitu. Zalecane jest używanie tylko jednego akapitu na raz. Przed rozpoczęciem pracy należy określić zmienną N_SEQ nadając jej wartość równą liści łańcuchów używanych w analizie **( np. a=$(wc -l 1.0.bezspacji | awk '{print $1}'); echo $[$a/3])**. Następnie „zahaszowujemy” jeden z akapitów i możemy odpalać skrypt. Pierwszy akapit tworzy drzewo filogenetyczne zależności między łańcuchami oraz historię łączenia skupień ze sobą. Drugi natomiast drugi tworzy skupienia w postaci oddzielnych plików.

*Zastrzeżenie praktyczne: na ostatni krok warto wykonać w podkatalogu z uwagi, że run_clustering.sh tworzy je bezpośrednio do miejsca gdzie się znajduje i powstaje całkiem dużo plików co pogarsza nawigację.*

**5.3** Otrzymane skupienia można przekonwertować za pomocą retrieve_clusters.py po formatu fasta, gdyby zaszła taka potrzeba.

Uwagi końcowe i dodatkowe pliki:
Mam nadzieję, że opis jest możliwe wyczerpujący i rozumiały. W razie pytań zostawiam mail-a:
**mateusz.sklodowski@student.uw.edu.pl.** W repozytorium zostawiam również pliki z rozszerzeniem .pbs. 
Są to pliki do systemu kolejkowania klastra funk
PS. Jeżeli jakiś program nie będzie działać, proponuje napierw użyć *chmod +x program.sh*. 
