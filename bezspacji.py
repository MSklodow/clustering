from pybioshell.core.data.io import read_fasta_file, create_fasta_string
import sys

def convert_list_to_string(org_list, seperator=' '):
    return seperator.join(org_list)

def parse(iinput):
    listagrup = []
    listaunp = read_fasta_file(iinput)
    out_name = iinput[0:4] + "bezspacji"
    out = open(out_name, "w")
    for listaseq in listaunp:
        h = listaseq.header().split()
        ssq = listaseq.sequence
        out.write(">")
        for index in range(0,len(h)//2):
           out.write(h[index*2] + h[index*2+1] + " ")
        out.write("\n"+ssq + "\n" + "\n" )
    out.close()

if __name__ == "__main__":
    parse(sys.argv[1])
