import sys
import subprocess
import re
from pybioshell.core.data.io import read_fasta_file, create_fasta_string
if len(sys.argv) < 1:
  print("""

Przyjmuje plik z wieloma sekwencjami fasta (pogrupowane) i 
przez blast szuka najbliĹĽszych podobnych(evalue) w postaci tabeli bialko query, bialko bazy danych


UĹĽycie 
    python3 StHLA input.fasta 

Uwaga: Do pracy pogramu potrzebny jest blast oraz spsyblash.sh - skrypt z inputem do blasta.


  """)
  sys.exit()


class Seq:
    def __init__(self):
        self.pdbID = ()
        self.lista = []

        self.name = ()
        self.dic = {}
        self.pdblist = []


class _RegExLib:
   _reg_qpdb = re.compile(r"(\w{5})\s{6,}-\s{6,}\w{5}")  #Szuka pary - kod pdb

   def __init__(self, line):
       self.qpdb = _RegExLib._reg_qpdb.search(line)


def len_of_file(iinput):
    line_count = 0
    with open(iinput, "r") as file:
        line = file.readline()
        while line:
            if ">" in line:
                line_count += 1
            line = file.readline()
    file.close()
    return line_count


def parse(iinput):
    listagrup = []
    listaunp = read_fasta_file(iinput)
    i=0
    for listaseq in listaunp:
        i+=1
        h = listaseq.header().split()
        ssq = listaseq.sequence
        t = Seq()
        t.lista = h
        t.pdbID = h[0]
        print(t.pdbID + " Wykonano " + str(i) + "/" + str(len_of_file(iinput)))
        cmd = ['./sjackhmmer.sh', t.pdbID, ssq]
        subprocess.run(cmd)
        with open(t.pdbID + ".out", "r") as file:
            line = file.readline()
            while line:
                reg_match = _RegExLib(line)
                qpdb_reg = reg_match.qpdb
                if qpdb_reg is not None:
                    qpdb_regq = qpdb_reg.group(1)
                    qpdb_regqq = str(qpdb_regq).replace(" ", "")
                    t.pdblist.append(qpdb_regqq)
                line = file.readline()
        if t is not None:
            listagrup.append(t)
        else:
            break
        cmd2 = ['./deleter.sh']
        subprocess.run(cmd2)
    out_name = iinput[0:4] + "data__input"
    out = open( out_name, "w")
    out.write("#" + "Query_PDB_id" + " " + "Hit_PDB_id"+"\n" )
    for klastry in range(0, len(listagrup)):
        for pdbcount in listagrup[klastry].pdblist:
            print(pdbcount)
            if pdbcount == listagrup[klastry].pdbID:
                continue
            out.write(listagrup[klastry].pdbID + " " + pdbcount + "\n")
    out.close()
if __name__ == "__main__":
    parse(sys.argv[1])


