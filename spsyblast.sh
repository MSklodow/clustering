#!/bin/bash
TARGET=$1
CHAIN=$2
SEQ=$3
echo -e "> $TARGET $CHAIN \n $SEQ" > $TARGET.fasta

/mnt/storage/TOOLS/blast+/bin/psiblast -num_iterations 5 \
  -num_alignments 20000 \
  -num_descriptions 10000 \
  -max_hsps 20000 \
  -inclusion_ethresh 0.1 \
  -evalue 0.1 \
  -db /1.0/1.0DB/1.0 \
  -query ./$TARGET.fasta \
  -show_gis  -outfmt 5 \
  -num_threads 1 \
  -out ./$TARGET.psi \
  
sed 's/<Hit_accession>/<Hit_temp>/' $TARGET.psi | sed 's/<\/Hit_accession>/<\/Hit_temp>/' | sed 's/<Hit_def>/<Hit_accession>/' | sed 's/<\/Hit_def>/<\/Hit_accession>/' | sed 's/<Hit_temp>/<Hit_id>/' | sed 's/<\/Hit_temp>/<\/Hit_id>/' > $TARGET.temp.psi
sleep 0.5
mv $TARGET.psi $TARGET??.??.psi
mv $TARGET.temp.psi $TARGET.psi
~/bioshell/doc_examples/cc-examples/core/data/ap_blastxml_to_hsp/ap_blastxml_to_hsp $TARGET.psi > $TARGET.hsp
