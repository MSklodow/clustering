from pybioshell.core.data.io import read_fasta_file, create_fasta_string
import sys
import os
import glob

if __name__ == "__main__":
    if len(sys.argv) <= 1:
        print("""

    Przyjmuje plik z wieloma sekwencjami fasta(bezspacji) i dzieli je na N podplikow z plikami

    Uzycie
        python3 fastaspliter.py Listafast N

    Uwaga: Do pracy pogramu potrzebny pybioshell.so.


      """)
        sys.exit()
        
def split(iinput, N):
    listaunp = read_fasta_file(iinput)
    i = 0
    for listaseq in listaunp:
        h = listaseq.header().split()
        ssq = listaseq.sequence
        filename = "T" + str(iinput) + "_split_" + str(i)
        f = open(filename, "a")
        f.write(">")
        for index in range(0, len(h)):
            f.write(h[index] + " ")
        f.write("\n" + ssq + "\n" + "\n")
        i += 1
        if int(N) == int(i):
            i = 0




    #Usuwanieplikow
    files = glob.glob(os.getcwd() + "/T" + sys.argv[1] + '_split_*', recursive=True)


    for fa in files:
        try:
            os.remove(fa)
        except OSError as e:
            print("Error: %s : %s" % (fa, e.strerror))

    #split(sys.argv[1], sys.argv[2])
