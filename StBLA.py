import sys
import subprocess
import re
from pybioshell.core.data.io import read_fasta_file, create_fasta_string
if len(sys.argv) < 1:
  print("""

Przyjmuje plik z wieloma sekwencjami fasta (pogrupowane) i 
przez blast szuka najbliższych podobnych(evalue) w postaci tabeli Białko QueryBiałko 


Użycie 
    python3 StBLA.py input.fasta 

Uwaga: Do pracy pogramu potrzebny jest blast,deleter.sh spsyblash.sh - skrypt z inputem do blasta.


  """)
  sys.exit()


class Seq:
    def __init__(self):
        self.pdbID = ()
        self.lista = []
        self.chain = ()
        self.dic = {}
    def pprint(self, lista):
        iidb = 0
        listaiidb = []
        for idb in lista:
            if iidb % 2 == 0:
                listaiidb.append(idb)
                iidb += 1
            else:
                iidb += 1
        return listaiidb


class _RegExLib:
   _reg_qpdb = re.compile(r"\[ *(\w* \w)]")  #Szuka pary - kod pdb
   _reg_evalue = re.compile(r"\(\s?(\S{1,3})%\)\s{3}\d\.\d\de") #Szuka pary - %indent

   def __init__(self, line):
    self.qpdb = _RegExLib._reg_qpdb.search(line)
    self.evalue = _RegExLib._reg_evalue.search(line)


def len_of_file(iinput):
    line_count = 0
    with open(iinput, "r") as file:
        line = file.readline()
        while line:
            if ">" in line:
                line_count += 1
            line = file.readline()
    file.close()
    return line_count


def parse(iinput):
    listagrup = []
    listaunp = read_fasta_file(iinput)
    i=0
    for listaseq in listaunp:
        i+=1
        h = listaseq.header().split()
        ssq = listaseq.sequence
        t = Seq()
        t.lista = h
        t.pdbID = h[0]
        t.chain = h[1]
        print(t.pdbID + " Wykonano " + str(i) + "/" + str(len_of_file(iinput)))
        cmd = ['./spsyblast.sh', t.pdbID, t.chain, ssq]
        subprocess.run(cmd)
        with open(t.pdbID + ".hsp", "r") as file:
            line = file.readline()
            while line:
                reg_match = _RegExLib(line)
                qpdb_reg = reg_match.qpdb
                qpdb_regq = qpdb_reg.group(1)
                qpdb_regqq = str(qpdb_regq).replace(" ","")
                evalue_reg = reg_match.evalue
                if qpdb_reg is not None and evalue_reg is not None:
                    if qpdb_regqq in t.dic:
                        if t.dic[qpdb_regqq] > evalue_reg.group(1):
                            t.dic[qpdb_regqq] = evalue_reg.group(1)
                    else:
                        t.dic[qpdb_regqq] = evalue_reg.group(1)
                line = file.readline()
        if t is not None:
            listagrup.append(t)
        else:
            break
    out_name = iinput[0:4] + "data__input"
    out = open( out_name, "w")
    k=400
    out.write("#" + "Query_PDB_id" + " " + "Hit_PDB_id"+ " " + "seq_%id" +"\n" )
    for klastry in range(0, len(listagrup)):
        for key, idnvalue in listagrup[klastry].dic.items():
            if key[0:4] == listagrup[klastry].pdbID:
                continue
            seq_ident2 = 1 - int(idnvalue) / 100
            stseq_ident2 = str("{:7.3f}".format(seq_ident2))
            seq_ident = (1 - int(idnvalue) / 100) * k
            stseq_ident = str("{:3.0f}".format(seq_ident))
            out.write(listagrup[klastry].pdbID + listagrup[klastry].chain + " " + key + " " +"\n")
            #out.write(listagrup[klastry].pdbID + listagrup[klastry].chain + " " + key + " " + str(idnvalue) + " " + stseq_ident2 +"\n")
        cmd2 = ['./deleter.sh']
        subprocess.run(cmd2)
    out.close()
if __name__ == "__main__":
    parse(sys.argv[1])
